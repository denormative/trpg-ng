var Combat = (function () {
    'use strict';

    function newMap() {
        var map = {
            width: 10,
            height: 10,
            locations: {}
        };

        return map;
    }

    function getAt(map, height, width) {
        // Todo: assert max
        return map.locations[height*map.width+width];
    }

    function newCombat(characters) {
        var combat = {
            characters: characters,
            enemies: [],
            round: null,
            currentCombatant: null
            //awarenessMap: newMap(),
            //visibilityMap: newMap()
        };

        combat.enemies.push(Enemy.newEnemy());
        return combat;
    }

    function fight(combat) {

    }

    function makeActionCheck(actionCheckScore) {
        var roll = Dice.d20();
        if (actionCheckScore.base.step != 0) {
            roll += Dice.roll(actionCheckScore.base.die)
        }

        return roll;
    }

    function newCombatRound(combat) {
        var round = {
            amazing: [],
            good: [],
            ordinary: [],
            marginal: []
        };

        combat.characters.forEach(function (c) {
            var roll = makeActionCheck(c.actionCheckScore);

            if (roll<= c.actionCheckScore.amazing.score) {
                round.amazing.push({score: roll, character: c, enemy: null, acted: false});
            } else if (roll<= c.actionCheckScore.good.score) {
                round.good.push({score: roll, character: c, enemy: null, acted: false});
            } else if (roll<= c.actionCheckScore.ordinary.score) {
                round.ordinary.push({score: roll, character: c, enemy: null, acted: false});
            } else {
                round.marginal.push({score: roll, character: c, enemy: null, acted: false});
            }
        });

        console.log(JSON.stringify(combat.enemies));
        combat.enemies.forEach(function (e) {
            //var roll = makeActionCheck(c.actionCheckScore);

            //TODO: shall we do proper action checks for monsters?
            switch(e.reaction.phase) {
                case "amazing":
                    round.amazing.push({score: 99, character: null, enemy: e, acted: false});
                    break;
                case "good":
                    round.good.push({score: 99, character: null, enemy: e, acted: false});
                    break;
                case "ordinary":
                    round.ordinary.push({score: 99, character: null, enemy: e, acted: false});
                    break;
                case "marginal":
                    round.marginal.push({score: 99, character: null, enemy: e, acted: false});
                    break;
            }
        });

        round.amazing = _.sortBy(round.amazing, 'score');
        round.good = _.sortBy(round.good, 'score');
        round.ordinary = _.sortBy(round.ordinary, 'score');
        round.marginal =  _.sortBy(round.marginal, 'score');

        combat.round = round;

        nextCombatant(combat);
    }

    function nextCombatant(combat) {
        console.log("next");
        combat.currentCombatant = null;

        combat.round.amazing.forEach(function(a) {
            if(combat.currentCombatant === null) {
                if(a.acted === false) {
                    combat.currentCombatant = a;
                }
            }
        });
        combat.round.good.forEach(function(a) {
            if(combat.currentCombatant === null) {
                if(a.acted === false) {
                    combat.currentCombatant = a;
                }
            }
        });
        combat.round.ordinary.forEach(function(a) {
            if(combat.currentCombatant === null) {
                if(a.acted === false) {
                    combat.currentCombatant = a;
                }
            }
        });
        combat.round.marginal.forEach(function(a) {
            if(combat.currentCombatant === null) {
                if(a.acted === false) {
                    combat.currentCombatant = a;
                }
            }
        });

        // if we're here, no-one else can act so new round?
        if(combat.currentCombatant === null) {
            newCombatRound(combat)
        }
    }

    return {
        fight: fight,
        newCombat: newCombat,
        newCombatRound: newCombatRound,
        nextCombatant: nextCombatant
    };
})();
