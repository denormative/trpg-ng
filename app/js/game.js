var Game = (function () {
    'use strict';

    function newSettings() {
        return  {
            key: 'settings',
            state: "",
            time: 0,
            dataCollected: 0,
            dataSpent: 0,
            moneyCollected: 0,
            moneySpent: 0,
            clicks: 0,
            saveVersion: 1,
            version: 1
        };
    }

    function newCombat() {
        return {
            key: 'combat'
        };
    }

    function newParty() {
        var myParty = {key: 'party', characters: []};

        myParty.characters.push(Character.newCharacter());
        myParty.characters.push(Character.newCharacter());
        myParty.characters.push(Character.newCharacter());
        myParty.characters.push(Character.newCharacter());
        myParty.characters.push(Character.newCharacter());
        myParty.characters.push(Character.newCharacter());

        return myParty;
    }

    var Game = function () {
        var s = ObjectStorage.load("settings");
        if(s === null) {
            this.settings = newSettings();
        }
        else {
            this.settings = s;
        }

        var g = ObjectStorage.load("game");
        if(g === null) {
            this.resetGame();
        }
        else {
            this.party = g.party;
            this.combat = g.combat;
        }

        this.loaded = true;
    };

    Game.prototype.newGame = function () {
        this.party = newParty();
        this.combat = null; // created in Combat
        this.save();
    };

    Game.prototype.resetGame = function () {
        this.party = null;
        this.combat = null;
        this.save();
    };

    Game.prototype.save = function () {
        ObjectStorage.save('settings', this.settings);

        // Save every object's state to local storage
        var game = {
            //adventure: this.adventure,
            party: this.party,
            combat: this.combat
        };

        ObjectStorage.save('game', game);
    };

    return {Game: Game};
}());
