/** @module Helpers
 * Define some useful helpers that are used throughout the game.
 */
var CombatActions = (function () {
    'use strict';

    function debugSkip(combat) {
        console.log("skip");
        if (combat.currentCombatant !== null) {
            console.log(JSON.stringify(combat.currentCombatant.acted));
            combat.currentCombatant.acted = true;
            console.log(JSON.stringify(combat.currentCombatant.acted));
        }

        Combat.nextCombatant(combat);
    }
    return {
        debugSkip: debugSkip
    };
})();
