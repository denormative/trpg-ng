var Character = (function () {
    'use strict';

    var careers = [
        {
            name: "Bodyguard",
            profession: "Combat Spec"
        },
        {
            name: "Brawler",
            profession: "Combat Spec"
        },
        {
            name: "Corporate Security Specialist",
            profession: "Combat Spec"
        },
        {
            name: "Gunner",
            profession: "Combat Spec"
        },
        {
            name: "Law Enforcer",
            profession: "Combat Spec"
        },
        {
            name: "Martial Artist",
            profession: "Combat Spec"
        },
        {
            name: "Mercenary",
            profession: "Combat Spec"
        },
        {
            name: "Soldier",
            profession: "Combat Spec"
        },
        {
            name: "Spacehand",
            profession: "Combat Spec"
        },
        {
            name: "Ambassador",
            profession: "Diplomat"
        },
        {
            name: "Cleric",
            profession: "Diplomat"
        },
        {
            name: "Entertainer",
            profession: "Diplomat"
        },
        {
            name: "First Contact Consul",
            profession: "Diplomat"
        },
        {
            name: "Trader",
            profession: "Diplomat"
        },
        {
            name: "Bounty Hunter",
            profession: "Free Agent"
        },
        {
            name: "Explorer",
            profession: "Free Agent"
        },
        {
            name: "Gambler",
            profession: "Free Agent"
        },
        {
            name: "Reporter",
            profession: "Free Agent"
        },
        {
            name: "Spy",
            profession: "Free Agent"
        },
        {
            name: "Comptech",
            profession: "Tech Op"
        },
        {
            name: "Doctor",
            profession: "Tech Op"
        },
        {
            name: "Engineer",
            profession: "Tech Op"
        },
        {
            name: "Hacker",
            profession: "Tech Op"
        },
        {
            name: "Pilot",
            profession: "Tech Op"
        },
        {
            name: "Scholar",
            profession: "Tech Op"
        },
        {
            name: "Scientist",
            profession: "Tech Op"
        }
    ];

    var professions = [
        {
            name: "Combat Spec",
            specialBenefits: {
                actionCheckScoreIncrease: +3
            }
        },
        {
            name: "Diplomat",
            specialBenefits: {
                actionCheckScoreIncrease: +1
            }
        },
        {
            name: "Free Agent",
            specialBenefits: {
                actionCheckScoreIncrease: +2
            }
        },
        {
            name: "Tech Op",
            specialBenefits: {
                actionCheckScoreIncrease: +1
            }
        },
        {
            name: "Mindwalker",
            specialBenefits: {
                actionCheckScoreIncrease: +1
            }
        }
    ];

    var species = [
        {
            name: "Human",
            specialAbilities: {
                baseActionCheckStep: 0
            }
        }
    ];

    function resistanceModifier(abilityScore) {
        if (abilityScore <= 4) return -2;
        if (abilityScore <= 6) return -1;
        if (abilityScore <= 10) return 0;
        if (abilityScore <= 12) return 1;
        if (abilityScore <= 14) return 2;
        if (abilityScore <= 16) return 3;
        if (abilityScore <= 18) return 4;
        return 5;
    }

    function newAbilities(species, profession) {
        var na = {
            str: {
                name: "Strength",
                sName: "STR",
                ssName: "S",
                score: null,
                untrained: null,
                resMod: null
            },
            dex: {
                name: "Dexterity",
                sName: "DEX",
                ssName: "D",
                score: null,
                untrained: null,
                resMod: null
            },
            con: {
                name: "Constitution",
                sName: "CON",
                ssName: "C",
                score: null,
                untrained: null,
                resMod: null
            },
            int: {
                name: "Intelligence",
                sName: "INT",
                ssName: "I",
                score: null,
                untrained: null,
                resMod: null
            },
            wil: {
                name: "Will",
                sName: "WIL",
                ssName: "W",
                score: null,
                untrained: null,
                resMod: null
            },
            per: {
                name: "Personality",
                sName: "PER",
                ssName: "P",
                score: null,
                untrained: null,
                resMod: null
            }
        };

        for (var key in na) {
            na[key].score = Dice.roll("2d6+2");
            na[key].untrained = Math.floor(na[key].score/2);
            na[key].resMod = resistanceModifier(na[key].score);
        }
        return na;

    }
    function newDurability(con) {
        var nd = {
            stun: {name: "Stun", sName: "STN", ssName: "S", score: 0, max: 0},
            wound: {name: "Wound", sName: "WND", ssName: "W", score: 0, max: 0},
            mortal: {name: "Mortal", sName: "MOR", ssName: "M", score: 0, max: 0},
            fatigue: {name: "Fatigue", sName: "FTG", ssName: "F", score: 0, max: 0}
        };

        // stun+wounds are equal to con
        nd.stun.score = nd.wound.score = con;
        nd.stun.max = nd.wound.max = con;
        // mortal+fatigue are equal to half of con rounded up
        nd.mortal.score = nd.fatigue.score = Math.ceil(con/2);
        nd.mortal.max = nd.fatigue.max = Math.ceil(con/2);

        return nd;
    }

    function newAttackForm() {
        var naf = {
            name: "",
            score: {ordinary: null, good: null, amazing: null},
            baseDice: {step: 0, die: ""},
            type: {form:"", firepower: ""},
            range: {short: 0, medium: 0, long: 0, personal: false},
            damage: {ordinary: "", good: "", amazing: ""}
        };

        return naf;
    }

    function toDiceString(dice, mod, type) {
        if (mod === 0) {
            return sprintf("%s%s", dice, type);
        }
        return sprintf("%s%+d%s", dice, mod, type);
    }

    function newUnarmedAttackForm(abilities) {
        var naf = newAttackForm();

        naf.name = "Unarmed";

        naf.score.ordinary = abilities.str.score;
        naf.score.good = Math.floor(abilities.str.score/2);
        naf.score.amazing = Math.floor(abilities.str.score/4);

        naf.baseDice.step = +1;
        naf.baseDice.die = stepToDie(naf.baseDice.step);

        naf.type = {form: "LI", firepower: "O"};

        naf.range.personal = true;

        naf.damage.ordinary = toDiceString("d4", abilities.str.resMod, "s");
        naf.damage.good = toDiceString("d4", abilities.str.resMod+1, "s");
        naf.damage.amazing = toDiceString("d4", abilities.str.resMod+2, "s");

        return naf;
    }

    function stepToDie(step) {
        switch(step) {
            case -5: return '-d20'; break;
            case -4: return '-d12'; break;
            case -3: return '-d8'; break;
            case -2: return '-d6'; break;
            case -1: return '-d4'; break;
            case  0: return '+d0'; break;
            case  1: return '+d4'; break;
            case  2: return '+d6'; break;
            case  3: return '+d8'; break;
            case  4: return '+d12'; break;
            case  5: return '+d20'; break;
            case  6: return '+2d20'; break;
            case  7: return '+3d20'; break;
        }
    }

    function actionsPerRound(abilities) {
        var apr = abilities.con.score + abilities.wil.score;

        if (apr<=15) return 1;
        if (apr<=23) return 2;
        if (apr<=31) return 3;
        return 4;
    }

    function newActionCheckScore(abilities, species, profession) {
        var nac = {
            marginal: {name: "Marginal", sName: "M", score: null},
            ordinary: {name: "Ordinary", sName: "O", score: null},
            good: {name: "Good", sName: "G", score: null},
            amazing: {name: "Amazing", sName: "A", score: null},
            base: {step: null, die: ""},
            actionsPerRound: null
        };

        var baseAC = Math.floor((abilities.dex.score + abilities.int.score) / 2)
            + profession.specialBenefits.actionCheckScoreIncrease;

        nac.marginal.score = baseAC+1;
        nac.ordinary.score = baseAC;
        nac.good.score = Math.floor(baseAC/2);
        nac.amazing.score = Math.floor(baseAC/4);
        nac.base.step = species.specialAbilities.baseActionCheckStep;
        nac.base.die = stepToDie(nac.base.step);
        nac.actionsPerRound = actionsPerRound(abilities);

        return nac;
    }

    var newCharacter = function () {
        var nc = {
            name: {full: "", short: ""},
            abilities: null,
            species: {
                name: ""
            },
            profession: {
                name: ""
            },
            career: {
                name: "",
                profession: ""
            },
            durability: null,
            actionCheckScore: null,
            attackForms: []
        };

        nc.name.short = chance.first();
        nc.name.full = nc.name.short + " " + chance.last();
        nc.career = chance.pick(careers);
        nc.profession = _.findWhere(professions, {name: nc.career.profession});//chance.pick(professions);
        nc.species = chance.pick(species);
        nc.abilities = newAbilities(nc.species, nc.profession);
        nc.durability = newDurability(nc.abilities.con.score);
        nc.actionCheckScore = newActionCheckScore(nc.abilities, nc.species, nc.profession);
        nc.attackForms.push(newUnarmedAttackForm(nc.abilities));

        //console.log(JSON.stringify(nc));
        return nc;
    };

    return {
        newCharacter: newCharacter
    };
})();
