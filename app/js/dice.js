var Dice = (function () {
    'use strict';

    var getRandomInt = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };

    var toInt = function (val) {
        return parseInt(val || "1");
    };

    var parseRoll = function (roll) {

        var prefix;
        var split_prefix = roll.match(/^([-+/*><])(.*)$/);

        if (split_prefix !== null) {
            prefix = split_prefix[1];
            roll = split_prefix[2];
        }

        var parts = roll.split(/d/);
        var sum = 0;

        if (parts.length !== 1) {
            var limit = toInt(parts[1]);
            for (var i = toInt(parts[0]) - 1; i >= 0; i--) {
                sum += getRandomInt(1, limit);
            }
        } else {
            sum = toInt(parts[0]);
        }
        return {
            prefix: prefix,
            value: sum
        };
    };

    var rollDices = function (spec) {
        return spec.replace(/[^-+/*><0-9d]+/g, "")
            .split(/(?=[-+><])/)
            .map(function (str) {
                return str.split(/(?=[\/*])/)
                    .map(parseRoll)
                    .reduce(function (a, b) {
                        switch (b.prefix) {
                            case "*":
                                return {
                                    prefix: a.prefix,
                                    value: a.value * b.value
                                };
                                break;
                            case "/":
                                return {
                                    prefix: a.prefix,
                                    value: Math.floor(a.value / b.value)
                                };
                                break;
                            default:
                                return -1;
                        }
                    });
            })
            .reduce(function (a, b) {
                switch (b.prefix) {
                    case "+":
                        return {value: a.value + b.value};
                        break;
                    case "-":
                        return {value: a.value - b.value};
                        break;
                    case ">":
                        return {value: (a.value > b.value) ? a.value : b.value};
                        break;
                    case "<":
                        return {value: (a.value < b.value) ? a.value : b.value};
                        break;
                    default:
                        return;
                }
            })
            .value;
    };
    function d20() {
        return getRandomInt(1, 20);
    }
    return {
        roll: rollDices,
        d20: d20
    };
})();
