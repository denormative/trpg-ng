var Enemy = (function () {
    'use strict';


    function newBaseEnemy() {
        var ne = {
            name: "",
            str: {die: "", value: 0},
            dex: {die: "", value: 0},
            con: {die: "", value: 0},
            int: {die: "", value: 0, animal: 0 },
            wil: {die: "", value: 0},
            per: {die: "", value: 0, animal: 0 },
            durability: {
                stun: 0,
                wound: 0,
                mortal: 0,
                fatigue: 0
            },
            actionCheck: {
                marginal: 0,
                ordinary: 0,
                good: 0,
                amazing: 0
            },
            move: {
                run: 0,
                walk: 0,
                swim: 0,
                fly: 0
            },
            actionsPerRound: 0,
            reaction: {phase: "", score: 0},
            attacks: []/*{
                name: "",
                score: {ordinary: null, good: null, amazing: null},
                range: {short: 0, medium: 0, long: 0, personal: false},
                type: {form:"", firepower: ""},
                damage: {ordinary: "", good: "", amazing: ""}
            }*/,
            resistance: {
                melee: 0,
                range: 0
            },
            armor: {
                li: "",
                hi: "",
                en: ""
            },
            skills: {},
            attack: null /* function */
        };

        return ne;
    }

    function newEnemy() {
        var ne = newBaseEnemy();

        ne.name = "Amphibian";
        ne.str = {value: 15, die: "d4+12"};
        ne.dex = {value: 8, die: "d4+5"};
        ne.con = {value: 17, die: "d4+15"};
        ne.int = {value: 1, die: "d4+2", animal: 4};
        ne.wil = {value: 8, die: "d4+5"};
        ne.per = {value: 1, die: "d4+2", animal: 4};
        ne.durability = {stun: 17, wound: 17, mortal: 9, fatigue: 9};
        ne.actionCheck = {marginal: 9, ordinary: 8, good: 4, amazing: 2};
        ne.move = {run: 20, walk: 4, swim: 20};
        ne.actionsPerRound = 2;
        ne.reaction = { phase: "marginal", score: 1};
        ne.attacks.push({
            name: "Tongue",
            score: {ordinary: 8, good: 4, amazing: 2},
            range: {short: 6, medium: 0, long: 0, personal: false},
            type: {form:"LI", firepower: "O"},
            damage: {ordinary: "d4s", good: "d4+1s", amazing: "d4+2s"}
        });
        ne.attacks.push({
            name: "Bite",
            score: {ordinary: 13, good: 6, amazing: 3},
            range: {short: 0, medium: 0, long: 0, personal: true},
            type: {form:"LI", firepower: "O"},
            damage: {ordinary: "d4+2w", good: "d6+3w", amazing: "d4m"}
        });
        ne.resistance =  { melee: +2, range: 0 };
        ne.armor = { li: "d4", hi: "d4-2", en: "d4-1" };
        // TODO: needs skills and attack function
        ne.attack = function () {
            // if were already engaged with a target
            if (this.engagedWith !== undefined) {
                // if distance to target is personal range, attack with personal range attack
                // else if target still visible
                    // if target in range of one ranged attack
                        // use attack
                    // else try to move into range and attack
                    // else guard
                // else try to move into range and attack
                // else guard

            }
            else {
                // find all visible targets
                // if in melee range with melee attack, attack with melee
                // if in range of range attack, attack closest
                // if can get in range if range attack, do so
                // if can get in range of melee attack, do so
                // if can't get in range of either, move closer and guard
            }
        };

        return ne;
    }

    return {
        newEnemy: newEnemy
    };
})();
