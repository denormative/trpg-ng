(function () {
    'use strict';

    Helpers.validateSaveVersion();

    var game = new Game.Game();

    var settings = game.settings;

    var lastSaved;

    angular.module('ngChance', [])
        .directive('ngChance', function () {
            return {
                restrict: 'A', //use as attribute
                link: function (scope, element, attrs) {
                    element.text(chance[attrs.ngChance]());
                }
            }
        });

    // Declare app level module which depends on views, and components
    var app = angular.module('myApp', [
        'ngAnimate',
        'myApp.version',
        'ngChance',
        'ui.router',
        'ui.bootstrap'
    ]);

    app.config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');

        $stateProvider
            .state('/', {
                url: '/',
                views: {
                    'main': {templateUrl: 'partials/home.html'},
                    'menu': {templateUrl: 'partials/home.menu.html'}
                }
            })
            .state('/party/all', {
                url: '/party/all',
                views: {
                    'main': {templateUrl: 'partials/party/all.html'},
                    'menu': {templateUrl: 'partials/party/all.menu.html'}
                }
            })
            .state('/party/character', {
                url: '/party/character/:id',
                params: {id: null},
                views: {
                    'main': {
                        templateUrl: 'partials/party/character.html',
                        controller: function ($scope, $stateParams, $log) {
                            //$log.info(JSON.stringify(game.party.characters));
                            $scope.character = game.party ? game.party.characters[$stateParams.id] : null;
                        }
                    },
                    'menu': {templateUrl: 'partials/party/character.menu.html'}
                }
            })
            .state('/combat/fight', {
                url: '/combat/fight',
                views: {
                    'main': {
                        templateUrl: 'partials/combat/fight.html',
                        controller: 'CombatController'
                    },
                    'menu': {templateUrl: 'partials/combat/fight.menu.html'}
                }
            })
    });

    app.controller('CombatController', function ($scope, $log, $state) {
        $scope.combatActions = CombatActions;

        $scope.fnord = function() {
            console.log("fnord");
        };
        $scope.newCombat = function() {
            game.combat = Combat.newCombat(game.party.characters);
            Combat.newCombatRound(game.combat);
        };

        $scope.game = game;

        $scope.map = new ROT.Map.Arena(10, 10);

        $scope.display1 = new ROT.Display({width: 10, height: 10, fontSize: 36, forceSquareRatio: true});

        var c = $scope.display1.getContainer();
        c.classList.add("center-block");
        document.getElementById("combatMap").appendChild(c);

        $scope.map.create(function (x, y, wall) {
            $scope.display1.draw(x, y, wall ? "#" : " ");
        });
    });

    app.directive('bsActiveLink', ['$location', function ($location) {
        return {
            restrict: 'A', //use as attribute
            replace: false,
            link: function (scope, elem) {
                //after the route has changed
                scope.$on('$routeChangeSuccess', function () {
                    var hrefs = ['/#' + $location.path(),
                        '#' + $location.path(), //html5: false
                        $location.path()]; //html5: true
                    angular.forEach(elem.find('a'), function (a) {
                        a = angular.element(a);
                        if (-1 !== hrefs.indexOf(a.attr('href'))) {
                            a.parent().addClass('active');
                        } else {
                            a.parent().removeClass('active');
                        }
                    });
                });
            }
        };
    }]);

    app.controller('SaveController',
        ['$scope', '$interval', function ($scope, $interval) {
            lastSaved = new Date().getTime();
            $scope.lastSaved = lastSaved;
            $scope.saveNow = function () {
                var saveTime = new Date().getTime();
                game.settings.time += saveTime - lastSaved;
                game.save();
                lastSaved = saveTime;
                $scope.lastSaved = lastSaved;
            };
            $scope.restart = function () {
                if (window.confirm(
                        'Do you really want to restart the game? All progress will be lost.'
                    )) {
                    ObjectStorage.clear();
                    window.location.reload(true);
                }
            };
            $interval($scope.saveNow, 10000);
        }]);

    app.filter('niceNumber', ['$filter', function ($filter) {
        return Helpers.formatNumberPostfix;
    }]);

    app.filter('toclass', function () {
        return function (cl) {
            switch (cl) {
                case "Stun":
                    return "text-info";
                    break;
                case "Wound":
                    return "text-warning";
                    break;
                case "Mortal":
                    return "text-danger";
                    break;
                case "Fatigue":
                    return "text-muted";
                    break;
            }
            return "";
        }
    });
    app.filter('spad', function () {
        return function (input, n) {
            if (input === undefined)
                input = "";
            if (input.length >= n)
                return input;
            var spaces = " ".repeat(n);

            return (spaces + input).slice(-1 * n)
        };
    });

    app.controller('StatsController', function ($scope) {
        $scope.settings = settings;
    });

    app.controller('GameController', function ($scope) {
        $scope.game = game;

        $scope.newGame = function() {
            $scope.game.newGame();
        }
    });

    app.controller('PartyController', function ($scope, $log, $state) {
        $scope.characters = game.party ? game.party.characters : null;
        //$log.info(JSON.stringify($scope.characters));
        $scope.OpenCourse = function (courseId) {
            alert('Called ' + courseId);
        };
        $scope.SelectCharacter = function (id) {
            $state.go('/party/character', {id: id});
        };
    });

    app.controller('mainController', function ($rootScope, $state) {

        // create the list of bootswatches
        $rootScope.bootstraps = [
            {name: 'Cerulean', url: 'cerulean'},
            {name: 'Cosmo', url: 'cosmo'},
            {name: 'Cyborg', url: 'cyborg'},
            {name: 'Darkly', url: 'darkly'},
            {name: 'Flatly', url: 'flatly'},
            {name: 'Journal', url: 'journal'},
            {name: 'Lumen', url: 'lumen'},
            {name: 'Paper', url: 'paper'},
            {name: 'Readable', url: 'readable'},
            {name: 'Sandstone', url: 'sandstone'},
            {name: 'Simplex', url: 'simplex'},
            {name: 'Slate', url: 'slate'},
            {name: 'Spacelab', url: 'spacelab'},
            {name: 'Superhero', url: 'superhero'},
            {name: 'United', url: 'united'},
            {name: 'Yeti', url: 'yeti'}
        ];

        // set the default bootswatch name
        $rootScope.css = {name: 'slate'};

        (function populateTheme() {
            var theme = localStorage.getItem("theme");
            if (theme === undefined || theme === null) {
                theme = 'cyborg';
            }

            $rootScope.css.name = theme;
        })();

        $rootScope.setTheme = function (theme) {
            // should have already happened, but... paranoid.
            $rootScope.css.name = theme;

            // save the new theme to $localStorage
            localStorage.setItem("theme", theme);
        };

        $rootScope.debugReset = function() {
            game.resetGame();
            $state.go('/');
        }
    });
})();
