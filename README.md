# README

Start of a simplish RPG developed in AngularJS with intent to learn the ins and outs of a SPA in Angular.

The general result was learning how not to write code in Angular. If I start working on it again, I'll probably just rip out all the business logic and re-code the UI properly. :/