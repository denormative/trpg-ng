module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      files: ['Gruntfile.js', 'app/app.js', 'app/js/*.js', 'app/home/*.js', 'app/view1/*.js', 'app/view2/*.js'],
      options: {
        globals: {
          jQuery: true,
	  browser: true,
          console: true,
          module: true,
          jasmine: true,
          angular: true,
	  describe: true,
	  it: true,
	  beforeEach: true,
	  inject: true,
	  expect: true,
        },
        reporter: require('jshint-stylish')
      }
    },
    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint']
    }/*,
    browserify: {
      dist: {
        src: 'node_modules/rot-js/lib/rot.js',
        dest: 'js/rot.js'
        // Note: The entire `browserify-shim` config is inside `package.json`.
      }
    }*/

  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  //grunt.loadNpmTasks('grunt-browserify');

  grunt.registerTask('default', ['jshint'/*, 'browserify'*/]);

};

